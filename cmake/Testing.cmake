set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_COMPILER g++)

find_package(GTest REQUIRED)

include(GoogleTest)

enable_testing()

add_library(wasted-core
STATIC
"${CMAKE_CURRENT_SOURCE_DIR}/src/utils/time.hpp"
)

# Optionally, specify any include directories to be exported
target_include_directories(wasted-core
    PRIVATE
    "${PROJECT_SOURCE_DIR}/lib/irrlicht/include"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/utils"
)

# Set the language explicitly to C++
set_target_properties(wasted-core PROPERTIES
    LINKER_LANGUAGE CXX
)

add_executable(wasted-tests
    ${CMAKE_CURRENT_SOURCE_DIR}/tests/mocks/mock_game_timer.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/tests/test_game_time.cpp
)

# Link with GTest and your project's libraries
target_include_directories(wasted-tests PRIVATE
    "${GTEST_INCLUDE_DIRS}"
    "${CMAKE_CURRENT_SOURCE_DIR}tests/mocks"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/utils/no_copy.hpp"
)

target_link_libraries(wasted-tests PRIVATE GTest::gtest_main ${GTEST_LIBRARIES} wasted-core)

gtest_discover_tests(wasted-tests)

