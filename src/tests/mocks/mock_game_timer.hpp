#ifndef __MOCK_GAME_TIMER_HPP__
#define __MOCK_GAME_TIMER_HPP__
#include <string>
#include <cstdint>
#include <memory>

#include "ITimer.h"
#include "irrTypes.h"

using namespace irr;

class MockGameTimer : public ITimer
{
private:
    std::unique_ptr<RealTimeDate> m_real_time_and_date;

public:

    MockGameTimer();

    ~MockGameTimer() {}

    virtual u32 getRealTime() const { return 0; }

    virtual RealTimeDate getRealTimeAndDate() const { return *m_real_time_and_date; }

    virtual void setRealTimeAndDate(RealTimeDate new_date)
    {
        m_real_time_and_date = std::make_unique<RealTimeDate>(new_date);
    }

    virtual f32 getSpeed () const { return 0; }

    virtual u32 getTime () const { return 0; }

    virtual bool isStopped () const { return false; }

    virtual void setSpeed (f32 speed=1.0f) { return; }

    virtual void setTime (u32 time) { return; }

    virtual void start () {};

    virtual void stop () {};

    virtual void tick () {};

};

#endif //__MOCK_GAME_TIMER_HPP__
