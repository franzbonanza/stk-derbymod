#include "tests/radio_station_test.hpp"
#include "config/player_manager.hpp"
#include "config/user_config.hpp"
#include "karts/abstract_kart.hpp"
#include "modes/free_for_all.hpp"
#include "network/network_config.hpp"
#include "network/rewind_manager.hpp"
#include "states_screens/state_manager.hpp"
#include "tracks/track.hpp"
#include "tracks/track_manager.hpp"

// ----------------------------------------------------------------------------

void RadioStationTest::testRadioMusicIsLoadedCorrectly()
{
    //RadioStationTest::resetState();
    // MUSIC IS BEING LOADED IN THE RADIO ARRAY FROM ALL RADIO STATIONS
    assert(!music_manager->getAllRadioMusic().empty());

    // A RANDOM RADIO STATION SHOULD HAVE BEEN SELECTED TOO, SO CHECK IF THE
    // RIGHT ARRAY IS BEING PROPERLY POPULATED
    assert(!music_manager->getSelectedRadioStationMusic().empty());
    // EXTRA CHECK THE SELECTED RADIO STATION NAME
    assert(!music_manager->getCurrentRadioStation().empty());

    // WHEN THE MUSIC MANAGER IS INITIALIZED ALL THE RADIO HISTORY SHOULD BE
    // EQUAL TO -1 BECAUSE NO MUSIC WAS PLAYED YET
    for(unsigned int i = 0; i < music_manager->radioStationsBacklog.size(); i++){
        assert(music_manager->radioStationsBacklog[i].first == -1);
        assert(music_manager->radioStationsBacklog[i].second == -1);
    }
}

// ----------------------------------------------------------------------------

void RadioStationTest::testChangeRadioStationSuccessfully()
{
    //RadioStationTest::resetState();
    std::string originalRadioStation = music_manager->getCurrentRadioStation();

    // TESTING NEXT STATION
    music_manager->nextRadioStation();
    assert(originalRadioStation != music_manager->getCurrentRadioStation());

    // TESTING PREVIOUS STATION
    originalRadioStation = music_manager->getCurrentRadioStation();
    music_manager->previousRadioStation();
    assert(originalRadioStation != music_manager->getCurrentRadioStation());
}
// ----------------------------------------------------------------------------

void RadioStationTest::testPlayRadioAndTestSeekWhenChangingRadioStation()
{
    RadioStationTest::resetState();
    // SET RACE -- MOVE TO DERBY TEST GENERIC FUNCTION?
    RaceManager::get()->setTimeTarget(500);
    RaceManager::get()->setMajorMode(RaceManager::MAJOR_MODE_SINGLE);
    RaceManager::get()->setMinorMode(RaceManager::MINOR_MODE_FREE_FOR_ALL);
    RaceManager::get()->setNumKarts(1);
    RaceManager::get()->setNumPlayers(1, 1);

    // ADD CARS
    for (int i = 0; i < 1; i++) {
        StateManager::get()->createActivePlayer(PlayerManager::getCurrentPlayer(), NULL);
        RaceManager::get()->setPlayerKart(i, UserConfigParams::m_default_kart);
    }
    RaceManager::get()->setupPlayerKartInfo();
    RaceManager::get()->setTrack("derby_track");
    RaceManager::get()->startNew(false);

    // CREATE WORLD
    FreeForAll *f = dynamic_cast<FreeForAll *>(FreeForAll::getWorld());
    f->setPhase(FreeForAll::RACE_PHASE);
    auto k = f->getKarts();
    k[0]->setEnable(true);

    // MUSIC STARTED BUT IT WON'T RUN BECAUSE THERE IS NO AUDIO IN TEST
    Track::getCurrentTrack()->startMusic();

    // DEFINE THE VALUE TO WHICH THE MUSIC SHOULD BE SEEKED
    int fakeSeek = 20;

    // SELECTED RADIO
    int radioStationIndex;

    // SET GAME TIME
    f->setTicksForRewind(0);

    // FIND OUT RIGHT RADIO STATION
    for (unsigned int i = 0; i < file_manager->getAllRadioStations().size(); i++) {
        if (file_manager->getAllRadioStations().at(i).compare(
                music_manager->getCurrentRadioStation())
            == 0) {
            radioStationIndex = i;
        }
    }

    // CHECK THAT WHEN THE MUSIC STARTED THE ELAPSED TIME IS NOT -1 ANYMORE
    // IN THE RADIO HISTORY ARRAY
    assert(music_manager->radioStationsBacklog[radioStationIndex].second != -1);

    // FAKE SEEK TRACK
    music_manager->radioStationsBacklog[radioStationIndex].second = fakeSeek;

    music_manager->nextRadioStation();

    for (unsigned int i = 0; i < file_manager->getAllRadioStations().size(); i++) {
        if (file_manager->getAllRadioStations().at(i).compare(
                music_manager->getCurrentRadioStation())
            == 0) {
            // FAKE SEEK ANOTHER TRACK
            music_manager->radioStationsBacklog[i].second = 10;
        }
    }

    // SET TIME FORWARD BY 1000 TICKS
    f->setTicksForRewind(1000);

    // WHEN CHANGING RADIO AGAIN, THE TIME PASSED SHOULD BE ADDED TO THE PREVIOUS
    // TRACK ELAPSED TIME. IF I CHANGED RADIO AT T = 20, AND I LISTEN FOR THE
    // NEW RADIO FOR 8 SECONDS, GOING BACK TO THE PREVIOUS RADIO MEANS T == 28.
    music_manager->previousRadioStation();

    //Track::getCurrentTrack()->nextRadioStation();
    assert(music_manager->radioStationsBacklog[radioStationIndex].second
           == fakeSeek + (int)stk_config->ticks2Time(f->getTicksSinceStart()));
}

void RadioStationTest::resetState(){
    STKProcess::reset();
    StateManager::clear();
    NetworkConfig::clear();
    RaceManager::clear();
    RaceManager::destroy();
    RaceManager::create();
    if (Track::getCurrentTrack()) {
        Track::getCurrentTrack()->cleanup();
    }
    RewindManager::destroy();
}
