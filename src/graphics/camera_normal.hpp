//
//  SuperTuxKart - a fun racing game with go-kart
//  Copyright (C) 2004-2015 Steve Baker <sjbaker1@airmail.net>
//  Copyright (C) 2006-2015 SuperTuxKart-Team, Steve Baker
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef HEADER_CAMERA_NORMAL_HPP
#define HEADER_CAMERA_NORMAL_HPP

#include "graphics/camera.hpp"
#include "utils/cpp2011.hpp"

/**
  * \brief Handles the normal racing camera
  * \ingroup graphics
  */
class CameraNormal : public Camera
{

private:

    /** Current ambient light for this camera. */
    video::SColor   m_ambient_light;

    /** Distance between the camera and the kart. */
    float           m_distance;

    /** Total rotation angle for camera rotation */
    float totalXRad;
    float totalYRad;

    float m_distance_increment;
    bool m_cam_free_mode;
    bool m_gamepad_rotation = false;
    float m_gamepad_rotation_x_axis;
    float m_gamepad_rotation_y_axis;

    /** The speed at which the camera changes position. */
    float           m_position_speed;

    /** The speed at which the camera target changes position. */
    float           m_target_speed;

    /** The speed at which the up-vector rotates, only used for the first person camera. */
    float m_angular_velocity;

    /** Height of camera **/
    float height;
    bool  smoothing;

    std::map<std::string, core::vector3df>m;
    std::map<std::string, core::vector3df>::iterator it;

    core::vector3df m_target_up_vector;
    core::vector3df m_lin_velocity;
    core::vector3df updatedDirection;
    core::vector3df local_direction;
    core::vector3df m_local_up;
    core::vector3df updatedPosition;
    core::vector3df m_target_pos;
    core::vector3df m_target_direction;
    bool m_attached;

    /** Factor of the effects of steering in camera aim. */
    float           m_rotation_range;

    Vec3            m_camera_offset;

    void moveCamera(float dt, bool smooth, float cam_angle, float distance);
    void handleEndCamera(float dt);
    void getCameraSettings(float *above_kart, float *cam_angle,
                           float *side_way, float *distance,
                           bool *smoothing, float *cam_roll_angle);
    
    void positionCamera(float dt, float above_kart, float cam_angle,
                        float side_way, float distance, float smoothing,
                        float cam_roll_angle);

    btVector3 m_kart_position;
    btQuaternion m_kart_rotation;

    // ---------------------------------
    // HELPER METHODS
    void setCamOffsetRelativeToCarMovement();
    // ---------------------------------
    // Give a few classes access to the constructor (mostly for inheritance)
    friend class Camera;
    friend class CameraDebug;
    friend class CameraEnd;
    friend class  CameraFps;
             CameraNormal(Camera::CameraType type, int camera_index,
                          AbstractKart* kart);
    virtual ~CameraNormal() {}
public:
    // ------------------------------------------------------------------------
    /** Applies rotation movement to the camera. */
    void applyCameraMovement (float x, float y);
    void setGamepadRotationValues (float x, float y, int deadzone);
    // ------------------------------------------------------------------------
    void setGamePadRotationState(float rotationActive) { m_gamepad_rotation = rotationActive; }
    // ------------------------------------------------------------------------
    void setGamepadXDirection(float x) { m_gamepad_rotation_x_axis = x; }
    // ------------------------------------------------------------------------
    void setGamepadYDirection(float y) { m_gamepad_rotation_y_axis = y; }
    // ------------------------------------------------------------------------
    void setCameraFreemode(bool status) { m_cam_free_mode = status; }
    //------------------------------------------------------------------------
    void setDistanceIncrement(float x){
        //if it exceeds value then set back to max
        if(m_distance_increment >= 3){
            m_distance_increment = 3;
        }
        //if it exceeds minimum value then set back to min
        else if (m_distance_increment <= -3){
            m_distance_increment = -3;
        }
        m_distance_increment += x;
    };
    // -----
    void snapToPosition();
     // ------------------------------------------------------------------------
    bool isDebug() { return false; }
    // ------------------------------------------------------------------------
    bool isFPS() { return false; }
    // ------------------------------------------------------------------------
    void updatePosition(core::vector3df position);
    // ------------------------------------------------------------------------
    virtual void update(float dt) OVERRIDE;
    // ------------------------------------------------------------------------
    /** Sets the ambient light for this camera. */
    void setAmbientLight(const video::SColor &color) { m_ambient_light=color; }
    // ------------------------------------------------------------------------
    void setDistanceToKart(float distance) { m_distance = distance; }
    // ------------------------------------------------------------------------
    float getDistanceToKart() const { return m_distance; }
    // ------------------------------------------------------------------------
    /** Returns the current ambient light. */
    const video::SColor &getAmbientLight() const {return m_ambient_light; }

    virtual void reverseCamera(bool reverse)  OVERRIDE;

    void cycleCamera(bool reverse);

};   // class CameraNormal

#endif

/* EOF */
