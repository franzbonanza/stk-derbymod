//  SuperTuxKart - a fun racing game with go-kart
//  Copyright (C) 2018 SuperTuxKart-Team
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include "modes/free_for_all.hpp"
#include "font/bold_face.hpp"
#include "font/font_manager.hpp"
#include "graphics/camera.hpp"
#include "graphics/camera_end.hpp"
#include "graphics/central_settings.hpp"
#include "graphics/explosion.hpp"
#include "graphics/irr_driver.hpp"
#include "graphics/render_info.hpp"
#include "graphics/stk_text_billboard.hpp"
#include "items/projectile_manager.hpp"
#include "karts/abstract_kart.hpp"
#include "karts/controller/controller.hpp"
#include "karts/controller/local_player_controller.hpp"
#include "karts/kart_model.hpp"
#include "karts/kart_properties.hpp"
#include "karts/kart_properties_manager.hpp"
#include "network/network_config.hpp"
#include "network/network_string.hpp"
#include "network/protocols/game_events_protocol.hpp"
#include "network/stk_host.hpp"
#include "tracks/track.hpp"
#include "tracks/track_object.hpp"
#include "tracks/track_object_manager.hpp"
#include "tracks/track_object_presentation.hpp"
#include "utils/string_utils.hpp"
#include "utils/translation.hpp"

#include <algorithm>
#include <utility>
#include <iostream>


// ----------------------------------------------------------------------------
/** Constructor. Sets up the clock mode etc.
 */
FreeForAll::FreeForAll() : WorldWithRank()
{
    if (RaceManager::get()->hasTimeTarget())
    {
        WorldStatus::setClockMode(WorldStatus::CLOCK_COUNTDOWN,
                                  RaceManager::get()->getTimeTarget());
    }
    else
    {
        WorldStatus::setClockMode(CLOCK_CHRONO);
    }
}   // FreeForAll

// ----------------------------------------------------------------------------
FreeForAll::~FreeForAll()
{
}   // ~FreeForAll

// ----------------------------------------------------------------------------
void FreeForAll::init()
{
    WorldWithRank::init();
    m_display_rank = false;
    m_count_down_reached_zero = false;
    m_use_highscores = false;

}   // init

// ----------------------------------------------------------------------------
/** Called when a battle is restarted.
 */
void FreeForAll::reset(bool restart)
{
    WorldWithRank::reset(restart);
    m_count_down_reached_zero = false;
    if (RaceManager::get()->hasTimeTarget())
    {
        WorldStatus::setClockMode(WorldStatus::CLOCK_COUNTDOWN,
                                  RaceManager::get()->getTimeTarget());
    }
    else
    {
        WorldStatus::setClockMode(CLOCK_CHRONO);
    }
    m_scores.clear();
    m_scores.resize(m_karts.size(), 100);
}   // reset

// ----------------------------------------------------------------------------
/** Called when the match time ends.
 */
void FreeForAll::countdownReachedZero()
{
    for(unsigned int i = 1; i < getNumKarts() ; i++){
        if(!getKartAtPosition(i)->hasFinishedRace()){
            getKartAtPosition(i)->finishedRace(RaceManager::get()->getTimeTarget());
        }
    }
    // Prevent negative time in network soccer when finishing
    m_time_ticks = 0;
    m_time = 0.0f;
    m_count_down_reached_zero = true;
}   // countdownReachedZero

// ----------------------------------------------------------------------------
/** Called when a kart is hit.
 *  \param kart_id The world kart id of the kart that was hit.
 *  \param hitter The world kart id of the kart who hit(-1 if none).
 */
bool FreeForAll::kartHit(int kart_id, int hitter)
{       
    if (NetworkConfig::get()->isNetworking() &&
        NetworkConfig::get()->isClient())
        return false;

    handleScoreInServer(kart_id, hitter);
    return true;
}   // kartHit

// ----------------------------------------------------------------------------
/** Called when the score of kart needs updated.
 *  \param kart_id The world kart id of the kart that was hit.
 *  \param hitter The world kart id of the kart who hit(-1 if none).
 */
// TO-DO REMOVE
void FreeForAll::handleScoreInServer(int kart_id, int hitter /*type*/)
{

}   // handleScoreInServer

// ----------------------------------------------------------------------------

void FreeForAll::setKartTextAndUpdateScore(NetworkString& ns){
    int kart_id = ns.getUInt8();
    float hp = ns.getFloat();
    float score = ns.getFloat();

    m_scores.at(kart_id) = score;
    irr::core::stringw stringToDisplay = "\n HP: ";
    stringToDisplay += hp;
    m_karts[kart_id]->setHp(hp);
    m_karts[kart_id]->setOnScreenText(stringToDisplay.subString(0,11), false);


    if(m_karts[kart_id]->getHp() <= 25){
        m_karts[kart_id]->showSmoke();
    }
    else
    {
        m_karts[kart_id]->resetParticles();
    }

    if(m_karts[kart_id]->getHp() <= 0){
        // WE HAVE TO ADD PARTICLES HERE BECAUSE EVERY CLIENT NEEDS TO KNOW
        // ABOUT THEM..
        HitEffect* he = new Explosion(m_karts[kart_id]->getXYZ(), "explosion",
                                      "explosion.xml");
        ProjectileManager::get()->addHitEffect(he);
        eliminateKart(kart_id, true);
    }
}

// ----------------------------------------------------------------------------
/** Returns the internal identifier for this race.
 */
const std::string& FreeForAll::getIdent() const
{
    return IDENT_FFA;
}   // getIdent

// ------------------------------------------------------------------------
void FreeForAll::update(int ticks)
{
    WorldWithRank::update(ticks);
    WorldWithRank::updateTrack(ticks);
    if (Track::getCurrentTrack()->hasNavMesh())
        updateSectorForKarts();
    calculateLeaderboard();
}   // update

// ----------------------------------------------------------------------------
/** Remove later. */
bool FreeForAll::isRaceOver()
{
    return false;
}   // isRaceOver

// ----------------------------------------------------------------------------
/** Returns the data to display in the race gui.
 */
void FreeForAll::getKartsDisplayInfo(
    std::vector<RaceGUIBase::KartIconDisplayInfo> *info)
{
    const unsigned int kart_amount = getNumKarts();
    for (unsigned int i = 0; i < kart_amount ; i++)
    {
        RaceGUIBase::KartIconDisplayInfo& rank_info = (*info)[i];
        rank_info.lap = -1;
        rank_info.m_outlined_font = true;
        rank_info.m_color = getColor(i);
        rank_info.m_text = getKart(i)->getController()->getName();
        if (RaceManager::get()->getKartGlobalPlayerId(i) > -1)
        {
            const core::stringw& flag = StringUtils::getCountryFlag(
                RaceManager::get()->getKartInfo(i).getCountryCode());
            if (!flag.empty())
            {
                rank_info.m_text += L" ";
                rank_info.m_text += flag;
            }
        }
        rank_info.m_text += core::stringw(L" (") +
                            StringUtils::toWString(getKart(i)->getHp()).subString(0,5) + L")";

    }
}   // getKartsDisplayInfo

// ----------------------------------------------------------------------------
void FreeForAll::terminateRace()
{
    // SET THE PODIUM OBJECTS AND THE KART COPIES
    const unsigned int kart_amount = getNumKarts();
    TrackObjectManager* tobjman = Track::getCurrentTrack()->getTrackObjectManager();
    TrackObject* currObj;
    core::vector3df default_rot(0, 0, 0);
    core::vector3df kart_podium_distance(0, 0.5, 0);

    // IF THE TRACK HAS OBJECTS, CHECK IF THERE ARE ALL 3 PODIUM OBJS
    if(!tobjman->getObjects().empty()) {
        for_in(currObj, tobjman->getObjects())
        {
            TrackObjectPresentationMesh* meshPresentation = currObj->getPresentation<TrackObjectPresentationMesh>();
            if (meshPresentation != NULL)
            {
                if (meshPresentation->getModelFile() == "gpwin_podium1.spm")
                    m_podium_steps[0] = currObj;
                else if (meshPresentation->getModelFile() == "gpwin_podium2.spm")
                    m_podium_steps[1] = currObj;
                else if (meshPresentation->getModelFile() == "gpwin_podium3.spm")
                    m_podium_steps[2] = currObj;
                else {
                    Log::warn("Podium", "Warning: object model '%s' not found, ignored.","gpwin_podium.spm");
                }
            }
            else {
                Log::warn("Podium", "Warning: no object model found.");
            }
        }
    }
    if (m_podium_steps[0] == NULL || m_podium_steps[1] == NULL || m_podium_steps[2] == NULL){
        Log::warn("Podium", "Warning: no podium object found in the track, using default podium!!");
        irr::scene::IMesh *m_podium = irr_driver->getMesh(
            file_manager->getAsset(FileManager::MODEL, "gpwin_podium.spm"));
        irr_driver->grabAllTextures(m_podium);
        for (int i = 0; i < 3; i++) {
            scene::ISceneNode *podium_node = irr_driver->addMesh(m_podium, "podium");
            podium_node->setScale(core::vector3df(2.5f, 1.0f, 1.0f));
            TrackObjectPresentationSceneNode *presentation2
                = new TrackObjectPresentationSceneNode(m_defaultPodiumSteps[i],
                                                       default_rot,
                                                       podium_node->getScale(),
                                                       podium_node);
            TrackObject *tobjNew = new TrackObject(m_defaultPodiumSteps[i],
                                                   default_rot,
                                                   podium_node->getScale(),
                                                   "ghost",
                                                   presentation2,
                                                   false /*isDynamic */,
                                                   NULL /*physics settings*/);
            m_podium_steps[i] = tobjNew;
            tobjman->insertObject(tobjNew);
        }
    }

    for (unsigned int i = 0; i < 3; i++)
    {
        // COPY THE KARTS AND MOVE THEM TO THE PODIUM
        const KartProperties* kp = NULL;
        AbstractKart* kart;
        if (i < kart_amount){
            kart = getKartAtPosition(i+1);
            kp = kart_properties_manager->getKart(kart->getIdent());
            KartModel* kart_model = kp->getKartModelCopy(std::make_shared<RenderInfo>(
                RaceManager::get()->getKartColor(kart->getWorldKartId()),
                false));

            m_all_kart_models.push_back(kart_model);
            scene::ISceneNode* kart_main_node = kart_model->attachModel(true, false);
            core::vector3df kart_pos;

            // WE WANNA CHECK IF THE MAP HAS ANY PODIUM POSITION PREDEFINED
            // APPLY DEFAULT IF IT'S NOT THE CASE
            if(m_podium_steps[i] != NULL){
            kart_pos = (m_podium_steps[i]->getAbsoluteCenterPosition() + kart_podium_distance);
            }
            else{
            kart_pos = (m_defaultPodiumSteps[i] + kart_podium_distance);
            }
            core::vector3df kart_scale(1.0f, 1.0f, 1.0f);
            TrackObjectPresentationSceneNode* presentation =
                new TrackObjectPresentationSceneNode(kart_pos, default_rot, kart_scale,
                                                     kart_main_node);
            TrackObject* tobj = new TrackObject(kart_pos, default_rot, kart_scale,
                                                "ghost", presentation, false /* isDynamic */, NULL /* physics settings */);
            tobjman->insertObject(tobj);
            m_kart_node[i] = tobj;

            // ADD PLAYER NAME ON THE KART COPY
            BoldFace* bold_face = font_manager->getFont<BoldFace>();
            STKTextBillboard* tb =
                new STKTextBillboard(
                    GUIEngine::getSkin()->getColor("red"),
                    GUIEngine::getSkin()->getColor("red"),
                presentation->getNode(), irr_driver->getSceneManager(), -1,
                    core::vector3df(0.0f, 1.9f, 0.0f),
                    core::vector3df(0.5f, 0.5f, 0.5f));

            if (CVS->isGLSL()){
                tb->init(getKartAtPosition(i+1)->getController()->getName(), bold_face);
            }
            else{
                tb->initLegacy(getKartAtPosition(i+1)->getController()->getName(), bold_face);
            }
            tb->drop();
        }
    }

    core::vector3df podiumPosition = m_podium_steps[0] != NULL ?
                                         m_podium_steps[0]->getAbsolutePosition() : m_defaultPodiumSteps[0];
    // CHANGE THE CAMERAS TO POINT TO THE PODIUM
    for (unsigned int i = 0; i < kart_amount ; i++){
        if(getKartAtPosition(i+1)->getController()->isLocalPlayerController()){
            // WE NEED TO CHANGE THE USERS CAMERA BACK TO CM_TYPE_END BECAUSE WHEN THEY
            // SPECTATE OTHER CARS, THE CAMERA GETS CHANGED TO CM_TYPE_NORMAL.
            // THE CAMERA GETS CHANGED TO END CAMERA AS SOON AS THE PLAYER IS ELIMINATED
            Camera::changeCamera(0, Camera::CM_TYPE_END);
            CameraEnd *camSpectator = (CameraEnd*)(Camera::getActiveCamera());
            // LET'S CHANGE CAMERA MODE TO PODIUM
            camSpectator->setMode(Camera::CM_PODIUM);
            // UPDATE THE POSITION TO POINT TO THE CENTRAL PODIUM OBJECT
            camSpectator->updatePosition(core::vector3df(podiumPosition.X,podiumPosition.Y+2,podiumPosition.Z+12));
            getKart(i)->finishedRace(0.0f, true/*from_server*/);
        }
        // IN CASE A SPECTATOR NEEDS TO CHANGE CAMERA TARGET TOO
        else {
            Camera::changeCamera(0, Camera::CM_TYPE_END);
            CameraEnd *camSpectator = (CameraEnd*)(Camera::getActiveCamera());
            camSpectator->setMode(Camera::CM_PODIUM);
            camSpectator->updatePosition(core::vector3df(podiumPosition.X,podiumPosition.Y+2,podiumPosition.Z+12));
        }
    }
    WorldWithRank::terminateRace();
}   // terminateRace

// ----------------------------------------------------------------------------

void FreeForAll::calculateLeaderboard()
{
    std::vector<std::pair<int, float> > ranks;
    // IF RACE IS NOT OVER ORDER BY HP
    if (WorldStatus::getPhase() == WorldStatus::RACE_PHASE)
    {
            for (unsigned i = 0; i < getNumKarts(); i++)
            {
                ranks.emplace_back(i, getKart(i)->getHp());
            }
    }
    // ELSE ORDER BY SCORE
    else{
            for (unsigned i = 0; i < m_scores.size(); i++)
            {
                // SCORE IS CALCULATED BY HAVING SCORES + TIME
                ranks.emplace_back(i, m_scores[i]);
            }
    }

    std::sort(ranks.begin(), ranks.end(),
              [](const std::pair<int, float>& a, const std::pair<int, float>& b)
              {
                  return a.second > b.second;
              });

    beginSetKartPositions();

    for (unsigned i = 0; i < ranks.size(); i++){
            setKartPosition(ranks[i].first, i + 1);
    }

    endSetKartPositions();

}

// ----------------------------------------------------------------------------

video::SColor FreeForAll::getColor(unsigned int kart_id) const
{
    return GUIEngine::getSkin()->getColor("font::normal");
}   // getColor

// ----------------------------------------------------------------------------
bool FreeForAll::getKartFFAResult(int kart_id)
{
    // the kart(s) which has the top score wins
    AbstractKart* k = getKartAtPosition(1);
    if (!k)
        return false;
    int top_score = getKartScore(k->getWorldKartId());
    return getKartScore(kart_id) == top_score;
}   // getKartFFAResult

// ----------------------------------------------------------------------------
void FreeForAll::saveCompleteState(BareNetworkString* bns, STKPeer* peer)
{
    for (unsigned i = 0; i < m_scores.size(); i++)
        bns->addUInt32(m_scores[i]);
}   // saveCompleteState

// ----------------------------------------------------------------------------
void FreeForAll::restoreCompleteState(const BareNetworkString& b)
{
    for (unsigned i = 0; i < m_scores.size(); i++)
        m_scores[i] = b.getUInt32();
}   // restoreCompleteState

void FreeForAll::eliminateKart(int kart_number, bool notify_of_elimination)
{
    assert(kart_number < (int)m_karts.size());
    AbstractKart *kart = m_karts[kart_number].get();
    if (kart->isGhostKart()) return;
    if(kart->getController()->isLocalPlayerController())
    {
        for(unsigned int i=0; i<Camera::getNumCameras(); i++)
        {
            // Change the camera so that it will be attached to the leader
            // and facing backwards.
            Camera *camera = Camera::getCamera(i);
            if(camera->getKart()==kart){
                camera->setMode(Camera::CM_NORMAL);
                // WHEN THE PLAYER DIES, ALWAYS ASSIGN A CAMERA
                // OF A PLAYER WHO IS STILL IN-GAME
                int k=0;
                auto kartAtIndex = m_karts.at(k);

                while (getKart(k)->getWorldKartId() == kart->getWorldKartId()){
                    k++;
                }
                camera->setKart(getKart(k));
                // Display a message about the eliminated kart in the race gui
                if (m_race_gui && notify_of_elimination){
                m_race_gui->addMessage(_("WASTED!"),
                                       camera->getKart(),
                                       2.5f,
                                       video::SColor(255, 255, 0, 0),
                                       true,
                                       true,
                                       true,
                                       true);
                }
                m_race_gui->addMessage(_("You are now spectating '%s'!",
                                         camera->getKart()->getController()->getName()),
                                       camera->getKart(),
                                       3.0f,
                                       video::SColor(255, 0, 230, 0),
                                       true,
                                       true,
                                       true,
                                       true);
            }
        }
        m_eliminated_players++;
    }

    //    // The kart can't be really removed from the m_kart array, since otherwise
    //    // a race can't be restarted. So it's only marked to be eliminated (and
    //    // ignored in all loops). Important:world->getCurrentNumKarts() returns
    //    // the number of karts still racing. This value can not be used for loops
    //    // over all karts, use RaceManager::get()->getNumKarts() instead!
    kart->explodeKart();
    m_eliminated_karts++;

}   // eliminateKart


// ----------------------------------------------------------------------------
std::pair<uint32_t, uint32_t> FreeForAll::getGameStartedProgress() const
{
    std::pair<uint32_t, uint32_t> progress(
        std::numeric_limits<uint32_t>::max(),
        std::numeric_limits<uint32_t>::max());
    if (RaceManager::get()->hasTimeTarget())
    {
        progress.first = (uint32_t)m_time;
    }
    AbstractKart* k = getKartAtPosition(1);
    float score = -1.0f;
    if (k)
        score = (float)getKartScore(k->getWorldKartId());

    if (score >= 0.0f)
    {
        progress.second = (uint32_t)(score /
                                      (float)RaceManager::get()->getHitCaptureLimit() * 100.0f);
    }
    return progress;
}   // getGameStartedProgress
