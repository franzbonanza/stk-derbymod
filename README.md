# WASTED!

<div align="center">

![GIF](data/gui/icons/logo_small.png)
</div>
<div align="center">

![status](https://img.shields.io/badge/status-alpha-red.svg)[![license](https://img.shields.io/badge/license-GPL--3.0-brightgreen.svg)](https://codeberg.org/franzbonanza/stk-derbymod/src/branch/main/COPYING)

[![matrix room](https://img.shields.io/badge/Chat%20on%20matrix%20-%23000.svg?&style=for-the-badge&logo=matrix&logoColor=white.svg)](https://matrix.to/#/%23wasted-dev%3Amatrix.org)

</div>

Wasted is a video game that features vehicular combat racing, developed by a cooperative of hobbyist game developers. Players engage in battle arenas where they can also use weapons or other tools to damage or destroy their opponents' vehicles. 

![GIF](https://codeberg.org/franzbonanza/stk-derbymod/raw/commit/10e94ad63b30132e1fa56a4f52281d9557bcc472/data/preview.gif)

## Hardware Requirements
To run Wasted, make sure that your computer's specifications are equal or higher than the following specifications:

* A graphics card capable of 3D rendering - NVIDIA GeForce 470 GTX, AMD Radeon 6870 HD series card or Intel HD Graphics 4000 and newer. OpenGL >= 3.3 (Or OpenGL 2.0 in legacy mode)
* You should have a dual-core CPU that's running at 1 GHz or faster.
* You'll need at least 512 MB of free VRAM (video memory).
* System memory: 1 GB
* Minimum disk space: 700 MB
* Ideally, you'll want a joystick with at least 6 buttons.

## Motivation 

In 2021, Franzbonanza started Wasted to fill the space left by GTA Derby Destruction, a vehicle-combat mod that was unfortunately discontinued. Franz shared his ideas on gaming forums and social media platforms, and formed a loose team from around the world while building the tools for a new game. Namely: CrystalTheEevee, who added car designs, and Spongycake, who supported gameplay. 

As more people joined the project in the following months, Franz then declared SuperTuxKart as the game's new base of development. Franz's reason was tied to the fact SuperTuxKart had an impressive toolset offering solid coding and testing environments. However, as a new battle mode was introduced, Wasted began to deviate from SuperTuxKart.  

Wasted remains committed to open source ideals, striving to produce a high-quality game that can both meet industry standards and become a popular hit. The team's development model is publicly available, and they actively encourage others to participate in the game's making. With this level of collaboration and dedication, the Wasted team is excited to showcase their game to the world and create an unforgettable experience for all players.

## License
The software is released under the GNU General Public License (GPL) which can be found in the file [`COPYING`](COPYING) in the same directory as this file. Information about the licenses for the artwork is contained in `data/licenses`.

---

## 3D coordinates
A reminder for those who are looking at the code and 3D models:

SuperTuxKart: X right, Y up, Z forwards

Blender: X right, Y forwards, Z up

The export utilities  perform the needed transformation, so in Blender you just work with the XY plane as ground, and things will appear fine in STK (using XZ as ground in the code, obviously).
## Building from source

You can try our new helper script in the folder of the game (stkHelper.sh).
That would be the easiest way to compile the game and start it. Keep in mind that it's an experimental "launcher" and works only for Debian based distros currently!
If you wish to do that manually you can follow the Building instructions in [`INSTALL.md`](INSTALL.md)

## Reach us!
Connect with us through our matrix space: https://matrix.to/#/%23wasted%3Amatrix.org?via=matrix.org

## Contributors

These are the important people whom aided in bringing the game to life.

### Programmers
Lead Developer - Franzbonanza

Developer -- Spongycake

### Visuals 
Race cars -- CrystalTheEevee

----

Special thanks to everyone working on SuperTuxKart. Their work has made our game possible. The story goes that Wasted used SuperTuxKart as a launchpad. For more details, visit [supertuxkart.net](https://supertuxkart.net/Team).
